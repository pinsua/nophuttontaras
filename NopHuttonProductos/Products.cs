﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopHuttonProductos
{
    class Products
    {
        
            public string articulo { get; set; }
            public string descrip { get; set; }
            public string unimed { get; set; }
            public decimal costo { get; set; }
            public decimal envaseKGfrac { get; set; }
            public string unimed2 { get; set; }
            public decimal envase { get; set; }
            public decimal precio { get; set; }
            public bool oferta { get; set; }
            public string bloqueo { get; set; }
            public decimal preciop { get; set; }
            public decimal ancho { get; set; }
            public decimal largo { get; set; }
            public string descrip2 { get; set; }
            public decimal gramaje { get; set; }
            public decimal fraccion { get; set; }
            public bool secorta { get; set; }
            public string nomtipoart { get; set; }
            public string pasoweb { get; set; }
            public decimal alto { get; set; }
            public decimal pesoesp { get; set; }
            public bool sefracciona { get; set; }
            public string proveedor { get; set; }

    }
}
