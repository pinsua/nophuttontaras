﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Prokey_Utils;
using System.ServiceModel;

namespace NopHuttonProductos
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string _nopUrl = ConfigurationManager.AppSettings["NopServiceUrl"];
            string _nopUser = ConfigurationManager.AppSettings["UsuarioNop"];
            string _nopPass = ConfigurationManager.AppSettings["PasswordNop"];
            string _lista = ConfigurationManager.AppSettings["NroLista"];
            string _connection = ConfigurationManager.ConnectionStrings["obdcPk"].ConnectionString;

            //Leemos la configuración.
            int _transporte = Convert.ToInt32(ConfigurationManager.AppSettings["TransporteDefault"]);
            int _tipoPed = Convert.ToInt32(ConfigurationManager.AppSettings["TipoPed"]);
            string _codigoFac = ConfigurationManager.AppSettings["CodigoFac"];
            int _sucursalSincro = Convert.ToInt32(ConfigurationManager.AppSettings["SucursalSincro"]);
            int _nroLista = Convert.ToInt32(ConfigurationManager.AppSettings["NroLista"]);
            string _direccionEnvio = ConfigurationManager.AppSettings["ErrorEmail"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["ErrorPassword"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ErrorSmtp"];
            string _puertoSmtp = ConfigurationManager.AppSettings["ErrorPort"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["ErrorCopia"];
            string _destinatarioEmail = ConfigurationManager.AppSettings["ErrorDestino"];
            string _nicEnvio = ConfigurationManager.AppSettings["ErrorNic"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["ErrorAsunto"];

            //string _sql = "SELECT        stock.articulo, stock.descrip, stock.\"unid-med\", stock.costo, stock.envaseKGfrac, stock.\"unid-med2\", stock.envase, stock.precio, stock.oferta, stock.bloqueo, stock.preciop, stock.ancho, stock.largo, stock.descrip2, stock.gramaje, " +
            //   "stock.fraccion, stock.secorta, rubros.NomTipoArt, stock.pasoWeb, stock.alto, stock.pesoesp, stock.sefracciona, prov.provn, marcas.marca_nom " +
            //   "FROM PUB.stock, PUB.rubros, PUB.prov, PUB.marcas " +
            //   "WHERE (stock.bloqueo = '0') AND (stock.talle = 'WEB') " +
            //   "AND (stock.tipoart = rubros.tipoart) AND (rubros.nivel = 2) AND (stock.proveedor = prov.proveedor) " +
            //   "AND (stock.marca_id = marcas.marca_id)"; //nivel 3 para rubro y tipoart

            string _sql = "SELECT        stock.articulo, stock.descrip, stock.\"unid-med\", stock.costo, stock.envaseKGfrac, stock.\"unid-med2\", stock.envase, stock.precio, stock.oferta, stock.bloqueo, stock.preciop, stock.ancho, stock.largo, stock.descrip2, stock.gramaje, " +
               "stock.fraccion, stock.secorta, rubros.NomTipoArt, stock.pasoWeb, stock.alto, stock.pesoesp, stock.sefracciona, prov.provn, marcas.marca_nom " +
               "FROM PUB.stock, PUB.rubros, PUB.prov, PUB.marcas " +
               "WHERE (stock.bloqueo = '0') AND (stock.temporada = 'WEB') " +
               "AND (stock.tipoart = rubros.tipoart) AND (rubros.nivel = 2) AND (stock.proveedor = prov.proveedor) " +
               "AND (stock.marca_id = marcas.marca_id)"; //nivel 3 para rubro y tipoart

            List<Products> _lst = new List<Products>();

            using (OdbcConnection _con = new OdbcConnection(_connection))
            {
                using (OdbcCommand _command = new OdbcCommand())
                {
                    _command.CommandType = CommandType.Text;
                    _command.Connection = _con;
                    _command.CommandText = _sql;

                    try
                    {
                        _con.Open();

                        using (OdbcDataReader _reader = _command.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    string _precio = _reader["precio"].ToString().Split(';')[2];
                                    string _preciop = _reader["preciop"].ToString().Split(';')[2];
                                    Products _cls = new Products();
                                    _cls.articulo = _reader.GetString(0);
                                    _cls.descrip = _reader.GetString(1);
                                    _cls.unimed = _reader.GetString(2);
                                    _cls.costo = _reader.GetDecimal(3);
                                    _cls.envaseKGfrac = _reader.GetDecimal(4);
                                    _cls.unimed2 = _reader.GetString(5);
                                    _cls.envase = _reader.GetDecimal(6);
                                    _cls.precio = Convert.ToDecimal(_precio, System.Globalization.CultureInfo.InvariantCulture);
                                    _cls.oferta = _reader.GetBoolean(8);
                                    _cls.bloqueo = _reader.GetString(9);
                                    _cls.preciop = Convert.ToDecimal(_preciop, System.Globalization.CultureInfo.InvariantCulture);
                                    _cls.ancho = _reader.GetDecimal(11);
                                    _cls.largo = _reader.GetDecimal(12);
                                    _cls.descrip2 = _reader.GetString(13);
                                    _cls.gramaje = _reader.GetDecimal(14);
                                    _cls.fraccion = _reader.GetDecimal(15);
                                    _cls.secorta = _reader.GetBoolean(16);
                                    _cls.nomtipoart = _reader.GetString(17);
                                    _cls.pasoweb = _reader.GetString(18);
                                    _cls.alto = _reader.GetDecimal(19);
                                    _cls.pesoesp = _reader.GetDecimal(20);
                                    _cls.sefracciona = _reader.GetBoolean(21);
                                    _cls.proveedor = _reader.GetString(22);

                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogFile.ErrorLog(LogFile.CreateV7Cli(), "Se produjo el siguiente error: " + ex.Message +
                            ". Leyendo los articulos" );
                        string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stocks no termino correctamente. Por favor reviselo.";

                        Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                            _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                    }
                    finally
                    {
                        if (_con.State == ConnectionState.Open)
                            _con.Close();
                    }
                }
            }

            using (NopService.NopServiceClient _nop = new NopService.NopServiceClient())
            {
                _nop.Endpoint.Address = new EndpointAddress(_nopUrl);

                foreach (Products pr in _lst)
                {
                    try
                    {
                        string _gramaje = Convert.ToInt32(pr.gramaje).ToString();
                        string _ancho = Convert.ToInt32(pr.ancho).ToString();
                        string _alto = Convert.ToInt32(pr.alto).ToString();
                        string _largo = Convert.ToInt32(pr.largo).ToString();
                        bool _nopRta = _nop.NewSingleProduct(_nopUser, _nopPass, pr.articulo, pr.descrip,
                            pr.descrip, pr.descrip2, "", pr.nomtipoart, pr.proveedor, _gramaje, pr.precio.ToString(), "0", pr.costo.ToString(),
                            pr.pesoesp.ToString(), _ancho, _alto, _largo, pr.fraccion);

                    }
                    catch (Exception ex)
                    {
                        LogFile.ErrorLog(LogFile.CreateV7Cli(), "Se produjo el siguiente error: " + ex.Message +
                            ". Procesando el articulo: " + pr.articulo);
                        string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stocks no termino correctamente. Por favor reviselo.";

                        Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                            _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                    }
                }

                LogFile.ErrorLog(LogFile.CreateV7Cli(), "Se termino de procesar los Articulos.");
            }
        }
    }
}
