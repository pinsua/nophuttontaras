﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Configuration;
using System.ServiceModel;

namespace NopHuttonStock
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Leemos la configuración.
            string _nopUrl = ConfigurationManager.AppSettings["NopServiceUrl"];
            string _nopUser = ConfigurationManager.AppSettings["UsuarioNop"];
            string _nopPass = ConfigurationManager.AppSettings["PasswordNop"];
            int _transporte = Convert.ToInt32(ConfigurationManager.AppSettings["TransporteDefault"]);
            int _tipoPed = Convert.ToInt32(ConfigurationManager.AppSettings["TipoPed"]);
            string _codigoFac = ConfigurationManager.AppSettings["CodigoFac"];
            int _sucursalSincro = Convert.ToInt32(ConfigurationManager.AppSettings["SucursalSincro"]);
            int _nroLista = Convert.ToInt32(ConfigurationManager.AppSettings["NroLista"]);
            string _direccionEnvio = ConfigurationManager.AppSettings["ErrorEmail"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["ErrorPassword"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ErrorSmtp"];
            string _puertoSmtp = ConfigurationManager.AppSettings["ErrorPort"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["ErrorCopia"];
            string _destinatarioEmail = ConfigurationManager.AppSettings["ErrorDestino"];
            string _nicEnvio = ConfigurationManager.AppSettings["ErrorNic"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["ErrorAsunto"];

            try
            {
                Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreatePath(), "Iniciando Sincronización de Stock con el eCommerce." );
                //Cargo la lista con todos los articulos a Sincronizar.
                List<StockSincro> _lstStock = StockSincro.GetData();

                if (_lstStock.Count > 0)
                {
                    //Recorro la lista.
                    foreach (StockSincro _ss in _lstStock)
                    {
                        int _warehouse = 0;
                        switch (_ss._sucNombre)
                        {
                            case "Buenos Aires": { _warehouse = 3; break; }
                            case "Rosario": { _warehouse = 1; break; }
                            case "Bahia Blanca": { _warehouse = 2; break; }
                            default: { _warehouse = 0; break; }
                        }

                        //Armo la cantidad en base a la fraccion.
                        //double _exist;
                        //if (_ss._fraccion > 0)
                        //    _exist = _ss._exist / _ss._fraccion;
                        //else
                        //    _exist = _ss._exist;
                        //int _unidades = Convert.ToInt32(_exist);
                        //Mandamos por Articulo,
                        //if (_warehouse > 0)
                        if (_warehouse == 3) //Cambiamos a solo Buenos Aires
                        {
                            decimal _stock = Convert.ToDecimal(_ss._exist);
                            //using (_srvNop380.NopServiceClient stock = new _srvNop380.NopServiceClient())
                            using (_srvNop380.NopServiceClient stock = new _srvNop380.NopServiceClient())
                            {
                                stock.Endpoint.Address = new EndpointAddress(_nopUrl);
                                bool booRta = stock.UpdateStock(_nopUser, _nopPass, _ss._articulo, _stock);
                                if (!booRta)
                                {
                                    //Escribo en el log.
                                    Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreatePath(), "El articulo " + _ss._articulo + " no se sincronizo.");
                                }
                            }
                        }
                    }
                    //Actualizo el sincrolog.

                }
                else
                { }
            }
            catch(Exception ex)
            {
                Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreatePath(), "Se produjo el siguiente error: " +ex.Message );

                string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stocks no termino correctamente. Por favor reviselo.";

                Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                    _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
            }
            Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreatePath(), "Finalizamos Sincronización de Stock con el eCommerce.");
        }
    }

    public class StockSincro
    {
        public string _articulo { get; set; }
        public int _sucursal { get; set; }
        public double _exist { get; set; }
        public string _sucNombre { get; set; }
        public double _fraccion { get; set; }

        public static List<StockSincro> GetData()
        {
            //string _sql = "SELECT PUB.inventa.articulo, PUB.inventa.sucursal, PUB.inventa.exist, PUB.sucursal.\"suc-nombre\", PUB.stock.fraccion " +
            //    "FROM PUB.inventa, PUB.stock, PUB.sucursal " +
            //    "WHERE PUB.inventa.articulo = PUB.stock.articulo AND PUB.inventa.sucursal = PUB.sucursal.sucursal " +
            //    "AND (PUB.stock.bloqueo = '0') AND (PUB.stock.talle = 'WEB') AND (PUB.inventa.sucursal = 1)";
            string _sql = "SELECT PUB.inventa.articulo, PUB.inventa.sucursal, PUB.inventa.exist, PUB.sucursal.\"suc-nombre\", PUB.stock.fraccion " +
                "FROM PUB.inventa, PUB.stock, PUB.sucursal " +
                "WHERE PUB.inventa.articulo = PUB.stock.articulo AND PUB.inventa.sucursal = PUB.sucursal.sucursal " +
                "AND (PUB.stock.bloqueo = '0') AND (PUB.stock.temporada = 'WEB') AND (PUB.inventa.sucursal = 1)";

            string _connectionString = ConfigurationManager.ConnectionStrings["obdcPk"].ConnectionString;

            List<StockSincro> _lst = new List<StockSincro>();

            try
            {

                using (OdbcConnection _conn = new OdbcConnection(_connectionString))
                {
                    _conn.Open();

                    using (OdbcCommand _cmd = new OdbcCommand())
                    {
                        _cmd.CommandText = _sql;
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.Connection = _conn;

                        using (OdbcDataReader _read = _cmd.ExecuteReader())
                        {
                            while (_read.Read())
                            {
                                if (_read.HasRows)
                                {
                                    StockSincro _cls = new StockSincro();
                                    _cls._articulo = _read.GetString(0);
                                    _cls._sucursal = _read.GetInt32(1);
                                    _cls._exist = _read.GetDouble(2);
                                    _cls._sucNombre = _read.GetString(3);
                                    _cls._fraccion = _read.GetDouble(4);

                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }
    }
}
