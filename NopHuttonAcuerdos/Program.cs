﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Configuration;
using System.IO;
using System.ServiceModel;

namespace NopHuttonAcuerdos
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Acuerdos.ProcesarAcuerdos(); 
        }
    }

    public class Acuerdos
    {
        public static void ProcesarAcuerdos()
        {
            DataSet _dts = new DataSet();
            string _path = ConfigurationManager.AppSettings["XmlAcuerdos"]; // "c:\\v7cli\\acuerdosNop.xml";
            string _userNop = ConfigurationManager.AppSettings["UsuarioNop"];
            string _passNop = ConfigurationManager.AppSettings["PasswordNop"];
            string _urlNop = ConfigurationManager.AppSettings["urlNop"];
            //Leemos la configuración.
            int _transporte = Convert.ToInt32(ConfigurationManager.AppSettings["TransporteDefault"]);
            int _tipoPed = Convert.ToInt32(ConfigurationManager.AppSettings["TipoPed"]);
            string _codigoFac = ConfigurationManager.AppSettings["CodigoFac"];
            int _sucursalSincro = Convert.ToInt32(ConfigurationManager.AppSettings["SucursalSincro"]);
            int _nroLista = Convert.ToInt32(ConfigurationManager.AppSettings["NroLista"]);
            string _direccionEnvio = ConfigurationManager.AppSettings["ErrorEmail"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["ErrorPassword"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ErrorSmtp"];
            string _puertoSmtp = ConfigurationManager.AppSettings["ErrorPort"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["ErrorCopia"];
            string _destinatarioEmail = ConfigurationManager.AppSettings["ErrorDestino"];
            string _nicEnvio = ConfigurationManager.AppSettings["ErrorNic"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["ErrorAsunto"];

            try
            {
                if (File.Exists(_path))
                {
                    _dts.ReadXml(_path);

                    foreach (DataRow dr in _dts.Tables[0].Rows)
                    {
                        try
                        {
                            string _articulo = dr["articulo"].ToString();
                            string _cliente = dr["cliente"].ToString();
                            string _precio = dr["precioArt"].ToString();
                            bool _rta = false;

                            //modifico el cliente, para que no aparezca al principio de ACL.
                            _cliente = "tp_" + _cliente;

                            if (Convert.ToDecimal(_precio) > 0)
                                _rta = true;
                            else
                                _rta = false;

                            if (_rta)
                            {
                                using (NopWService.NopServiceClient stock = new NopWService.NopServiceClient())
                                {
                                    stock.Endpoint.Address = new EndpointAddress(_urlNop);
                                    bool booRta = stock.SincroRol(_userNop, _passNop, _cliente, true);
                                    if (booRta)
                                    {
                                        bool _tPrice = stock.SincroTierPrice(_userNop, _passNop, _cliente, true, _articulo, 1, Convert.ToDecimal(_precio, System.Globalization.CultureInfo.InvariantCulture));
                                        if (_tPrice)
                                            Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "SincroOK: " + _cliente + ", " + _articulo);
                                        else
                                            Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "SincroError: " + _cliente + ", " + _articulo);
                                    }
                                    else
                                    {
                                        Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "SincroError: " + _cliente + ", No se creo el roll.");
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "Error: " + ex.Message);

                            //string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stocks no termino correctamente. Por favor reviselo.";

                            //Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                            //    _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                        }
                    }
                }
                else
                {
                    Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "No hay archivo de Acuerdos a Procesar.");                    
                }
            }
            catch (Exception ex)
            {
                Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "Error: " + ex.Message);

                string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stocks no termino correctamente. Por favor reviselo.";

                Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                    _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
            }
        }
    }
}
