﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.Odbc;
using System.ServiceModel;

namespace NopHuttonPrecios
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            string _nopUrl = ConfigurationManager.AppSettings["NopServiceUrl"];
            string _nopUser = ConfigurationManager.AppSettings["UsuarioNop"];
            string _nopPass = ConfigurationManager.AppSettings["PasswordNop"];
            string _lista = ConfigurationManager.AppSettings["NroLista"];
            //Leemos la configuración.
            int _transporte = Convert.ToInt32(ConfigurationManager.AppSettings["TransporteDefault"]);
            int _tipoPed = Convert.ToInt32(ConfigurationManager.AppSettings["TipoPed"]);
            string _codigoFac = ConfigurationManager.AppSettings["CodigoFac"];
            int _sucursalSincro = Convert.ToInt32(ConfigurationManager.AppSettings["SucursalSincro"]);
            int _nroLista = Convert.ToInt32(ConfigurationManager.AppSettings["NroLista"]);
            string _direccionEnvio = ConfigurationManager.AppSettings["ErrorEmail"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["ErrorPassword"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ErrorSmtp"];
            string _puertoSmtp = ConfigurationManager.AppSettings["ErrorPort"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["ErrorCopia"];
            string _destinatarioEmail = ConfigurationManager.AppSettings["ErrorDestino"];
            string _nicEnvio = ConfigurationManager.AppSettings["ErrorNic"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["ErrorAsunto"];

            try
            {
                Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "Iniciando Sincronización de Precios con el eCommerce.");
                //Cargo la lista con todos los articulos a Sincronizar.
                List<SincroPrecios> _lst = SincroPrecios.GetData();

                if (_lst.Count > 0)
                {
                    foreach (SincroPrecios _sp in _lst)
                    {
                        decimal _precioTienda = 0;
                        string[] arrPrecios = _sp._lstPrecios.Split(';');
                        int _listaPrecios = Convert.ToInt32(_nroLista) - 1;
                        decimal _precio = Convert.ToDecimal(arrPrecios[_listaPrecios], System.Globalization.CultureInfo.InvariantCulture);
                        if (_sp._unimed.ToUpper() == "KG" || _sp._unimed == "RS")
                        {
                            //Caculamos el valor del precio.
                            decimal _pregioKg = _precio / _sp._pesocom;
                            _precioTienda = Convert.ToDecimal(_pregioKg * _sp._pesoesp, System.Globalization.CultureInfo.InvariantCulture); ;
                        }
                        else
                            _precioTienda = _precio;
                        //Mandamos por Articulo,
                        using (_srvNop.NopServiceClient stock = new _srvNop.NopServiceClient())
                        {
                            stock.Endpoint.Address = new EndpointAddress(_nopUrl);
                            bool booRta = stock.UpdatePrice(_nopUser, _nopPass, _sp._articulo, _precioTienda, false);

                            if (!booRta)
                            {
                                //Escribo en el log.
                                Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "El articulo " + _sp._articulo + " no sincronizo su precio.");
                            }

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //Escribo en el log.
                Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "Se produjo el siguiente error: " + ex.Message);
                string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stocks no termino correctamente. Por favor reviselo.";

                Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                    _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
            }

            Prokey_Utils.LogFile.ErrorLog(Prokey_Utils.LogFile.CreateV7Cli(), "Finalización Sincronización de Precios con el eCommerce.");
        }
        public class SincroPrecios
        {
            public string _articulo { get; set; }
            public string _lstPrecios { get; set; }
            public bool _oferta { get; set; }
            public decimal _pesoesp { get; set; }
            public decimal _pesocom { get; set; }
            public string _unimed { get; set; }

            public static List<SincroPrecios> GetData()
            {
                //string _sql = "SELECT articulo, precio, oferta, pesocom, pesoesp, \"unid-med\" FROM PUB.stock WHERE(stock.bloqueo = '0') AND(stock.talle = 'WEB')";
                string _sql = "SELECT articulo, precio, oferta, pesocom, pesoesp, \"unid-med\" FROM PUB.stock WHERE(stock.bloqueo = '0') AND(stock.temporada = 'WEB')";

                string _connectionString = ConfigurationManager.ConnectionStrings["obdcPk"].ConnectionString;

                List<SincroPrecios> _lst = new List<SincroPrecios>();

                try
                {
                    using (OdbcConnection _conn = new OdbcConnection(_connectionString))
                    {
                        _conn.Open();

                        using (OdbcCommand _cmd = new OdbcCommand())
                        {
                            _cmd.CommandText = _sql;
                            _cmd.CommandType = System.Data.CommandType.Text;
                            _cmd.Connection = _conn;

                            using (OdbcDataReader _read = _cmd.ExecuteReader())
                            {
                                while (_read.Read())
                                {
                                    if (_read.HasRows)
                                    {
                                        SincroPrecios _cls = new SincroPrecios();
                                        _cls._articulo = _read.GetString(0);
                                        _cls._lstPrecios = _read.GetString(1);
                                        _cls._oferta = _read.GetBoolean(2);
                                        _cls._pesocom = _read.GetDecimal(3);
                                        _cls._pesoesp = _read.GetDecimal(4);
                                        _cls._unimed = _read.GetString(5);

                                        _lst.Add(_cls);
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _lst;
            }
        }
    }
}
