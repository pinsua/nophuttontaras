﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Prokey_Utils;

namespace NopHuttonPrecios
{
    class Email
    {
        /// <summary>
        /// Metodo para enviar un mail
        /// </summary>
        /// <param name="pAdjunto">Archivo adjunto con Path completo.</param>
        /// <param name="pDestinatario">Desinatario de mail, pueden ir varios.</param>
        /// <param name="pEnviador">Direccion de envio del mail.</param>
        /// <param name="pNicEnvaidor">Nick o texto a mostrar en el enviador.</param>
        /// <param name="pAsunto">Asunto del mail.</param>
        /// <param name="pBody">Texto del Mail.</param>
        /// <param name="pCredentials">Booleano que indica si se utilizan las credenciales.</param>
        /// <param name="pPuerto">Puerto de envio del mail.</param>
        /// <param name="pServidor">Servidor SMTP de salida.</param>
        /// <param name="pPassword">Password de la cuenta de envio.</param>
        /// <param name="pEnableSSL">Indica si el servidor utiliza SSL con Encriptacion.</param>
        /// <returns>True/False</returns>
        public static bool Enviar(string pAdjunto, string pDestinatario, string pDestinCopia, string pEnviador, string pNicEnvaidor,
            string pAsunto, string pBody, bool pCredentials, string pPassword, int pPuerto, string pServidor,
            bool pEnableSSL)
        {
            //Ruta de archivo adjunto (Si lo lleva)
            string PathFile = pAdjunto;

            //Creo el mensaje del mail.
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

            //Agrego los destinatarios, pueden ir varios
            msg.To.Add(pDestinatario);

            if (pDestinCopia.Trim().Length > 0)
            {
                //Agrego los destinatarios de Copia, pueden ir varios.
                msg.CC.Add(pDestinCopia);
            }
            //Agrego la direccion del que manda el email, puede ser falsa o verdadera, 
            //pero si es falsa puede que el servidor de correo lo detecte como spam, 
            //tambien depende de las credenciales que se ponen mas abajo
            msg.From = new MailAddress(pEnviador, pNicEnvaidor, System.Text.Encoding.UTF8);

            //Pongo el asunto
            msg.Subject = pAsunto;

            //El tipo de codificacion del Asunto 
            msg.SubjectEncoding = System.Text.Encoding.UTF8;

            //Escribo el mensaje Y su codificacion
            msg.Body = pBody;
            msg.BodyEncoding = System.Text.Encoding.UTF8;

            //Especifico si va ha ser interpertado con HTML
            msg.IsBodyHtml = true;

            //Agrego el archivo que puse en la ruta anterior "PathFile", y su tipo.
            if (PathFile.Trim().Length > 0)
            {
                string[] _adjuntos = PathFile.Split(';');
                foreach (string _at in _adjuntos)
                {
                    Attachment attachment = new Attachment(_at, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(_at);
                    disposition.ModificationDate = File.GetLastWriteTime(_at);
                    disposition.ReadDate = File.GetLastAccessTime(_at);
                    disposition.FileName = Path.GetFileName(_at);
                    disposition.Size = new FileInfo(_at).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    msg.Attachments.Add(attachment);
                }
            }
            //Creo un objeto de tipo cliente de correo (Por donde se enviara el correo)
            SmtpClient client = new SmtpClient();

            //Si no voy a usar credenciales pongo false, Pero la mayoria de servidores exigen las credenciales para evitar el spam
            client.UseDefaultCredentials = pCredentials;
            if (client.UseDefaultCredentials)
            {
                //Como voy a utilizar credenciales las pongo
                client.Credentials = new System.Net.NetworkCredential(pEnviador, pPassword);
            }
            //Si fuera gmail seria 587 el puerto, si es un servidor outlook casi siempre el puerto 25, yo utilizo un servidor propio de correo
            client.Port = pPuerto;

            //identifico el cliente que voy a utilizar
            client.Host = pServidor;

            //Si fuera a utilizar gmail esto deberia ir en true, esto es un certificado de seguridad
            client.EnableSsl = pEnableSSL;


            try
            {
                //Envio el mensaje
                client.Send(msg);
                return true;
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                LogFile.ErrorLog(LogFile.CreatePath(), "Error de envio de mail: " + ex.Message + "..." + ex.StackTrace);
                return false;
            }
        }
    }
}
